//
//  PhoneAuthController.swift
//  GoSpeed
//
//  Created by Marco CIccarelli on 19/04/2020.
//  Copyright © 2020 Marco CIccarelli. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseAuth

class PhoneAuthController: UIViewController {

    @IBOutlet weak var tf_phone_number: UITextField!
    @IBOutlet weak var tf_otp: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tf_otp.isHidden = true
        // Do any additional setup after loading the view.
    }
    

    var verification_id : String? = nil

    @IBAction func btn_submit(_ sender: Any) {
        
        if (tf_otp.isHidden) {
            if !tf_phone_number.text!.isEmpty {
                
                
                Auth.auth().settings?.isAppVerificationDisabledForTesting = false
                PhoneAuthProvider.provider().verifyPhoneNumber(tf_phone_number.text!, uiDelegate: nil, completion: {verificationID, error in
                    if (error != nil)
                    {
                        return
                        
                    }else {
                        self.verification_id = verificationID
                        self.tf_otp.isHidden = false
                    }
                })
            }else{
                print("Si prega di inserire il numero di telefono")
            }
            
        } else {
            if verification_id != nil {
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: verification_id!, verificationCode: tf_otp.text!)
                
                Auth.auth().signIn(with: credential, completion: {authData, error in
                    if (error != nil){
                        print(error.debugDescription)
                    }else {
                          print("Autenticazione avvenuta con successo ", (authData?.user.phoneNumber! ?? "Nessun numero di telefono"))
                    }
                })
            }else{
                print("Errore durante l'ottenimento dell'ID di verifica")
            }
                    
                    
               
        }
           
    }
}
