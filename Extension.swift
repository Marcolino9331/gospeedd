//
//  Extension.swift
//  GoSpeed
//
//  Created by Marco CIccarelli on 19/04/2020.
//  Copyright © 2020 Marco CIccarelli. All rights reserved.
//

import UIKit

extension UIViewController {

    func hideKeyboardWhenTappenAround(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard) )
        
        view.addGestureRecognizer(tapGesture)
    }
    
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}
